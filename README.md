# TasksWorkerApplication

This project is created in order to complete APD test. 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes:

* Branches:
            * master: this is the original or default branch. 
            * develop: this is the branch that contains test and the solution.
* Folder structure:
            * TasksWorkerApplication project: This project contains WPF application with unit test. 
            * ReadMe word document file: this file contains the steps and the approach of the proposed solution.


### Prerequisites

Please see the prerequisites below:
```
* Switch to develop branch.
* Clone the project.
* Microsoft Visual Studio "VS".
```

### Installing

1. Open Microsoft VS.
2. Open TasksWorkerApplication project.
3. build the solution using Ctrl+Shift+B . 
4. Run the unit tests.
5. Run the project using F5


## Running the tests

Use Test Explorer Window to run all the tests in the project.

## Versioning

The current version is: 1.0.0.0 .

## Author

* **Ismael Saifo** - [ESP Lead Developers](https://uk.linkedin.com/in/ismael-saifo-350bb348)

